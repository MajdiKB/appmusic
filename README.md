# appMusic

## Clone the repository in local folder.

`git clone git@gitlab.com:MajdiKB/appmusic.git`


## Install dependencies:

In the folder "front" (cd front)

`npm i`

## Run app:

`npm start`
