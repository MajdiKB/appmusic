import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: `artists`, loadChildren: () =>
      import('./pages/artists/artists.module').then(m => m.ArtistsModule)
  },
  {
    path: `albums`, loadChildren: () =>
      import('./pages/albums/albums.module').then(m => m.AlbumsModule)
  },
  {
    path: `home`, loadChildren: () =>
      import('./pages/home/home.module').then(m => m.HomeModule)
  },
  {
    path: `error`, loadChildren: () =>
      import('./core/error/error.module').then(m => m.ErrorModule)
  },
  { path: ``, redirectTo: `home`, pathMatch: `full` },
  { path: `**`, redirectTo: `home`, pathMatch: `full` },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
