import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlbumModelBack } from '../models/album-model';
import { Services } from '../../../shared/services/services.service';

@Component({
  selector: 'app-album-detail',
  templateUrl: './album-detail.component.html',
  styleUrls: ['./album-detail.component.scss'],
})
export class AlbumDetailComponent implements OnInit {
  public albumId: string = '';
  public artist: string = '';
  public artistId: string = '';
  public confirmDelete: boolean = false;
  public albumDetailData: AlbumModelBack = {} as AlbumModelBack;
  public artistAlbumsData: AlbumModelBack[] = [];

  constructor(
    private route: ActivatedRoute,
    private albumsService: Services,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.albumId = params.get('id') as string;
      this.getAlbumDetail(this.albumId);
      this.albumsService.goToTop();
    });
  }

  public getAlbumDetail(albumId: string): void {
    this.albumsService.getDetailAlbum(albumId).subscribe((data: AlbumModelBack) => {
      this.albumDetailData = data;
      this.albumsService
        .getArtistOfAlbum(this.albumDetailData.artistId)
        .subscribe(
          (dataArtist: any) => {
            if (dataArtist) {
              this.artist = dataArtist.name;
              this.artistId = dataArtist._id;
              this.albumsService
                .getAlbumsofArtist(dataArtist._id)
                .subscribe((dataAlbums: any) => {
                  this.artistAlbumsData = dataAlbums;
                  this.artistAlbumsData = this.artistAlbumsData.filter((item) => {
                    return item._id !== this.albumId;
                  });
                });
            } else {
              console.log(
                `The artist of the album ${this.albumDetailData.title} was deleted.`
              );
            }
          },
          (err) => console.log(err)
        );
    });
  }

  public deleteAlbum(albumId: string): void {
    this.albumsService.deleteAlbum(albumId).subscribe();
    setTimeout((): void  => {
      this.router.navigate(['./albums/']);
    }, 200);
  }
}
