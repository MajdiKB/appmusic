import { AlbumUpdateComponent } from './album-update/album-update.component';
import { AlbumCreateComponent } from './album-create/album-create.component';
import { AlbumDetailComponent } from './album-detail/album-detail.component';
import { AlbumsListComponent } from './albums-list/albums-list.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', component: AlbumsListComponent },
  { path: 'details/:id', component: AlbumDetailComponent },
  { path: 'new', component: AlbumCreateComponent },
  { path: 'update/:id', component: AlbumUpdateComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlbumsRoutingModule { }
