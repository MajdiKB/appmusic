import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlbumsRoutingModule } from './albums-routing.module';
import { AlbumsListComponent } from './albums-list/albums-list.component';
import { AlbumDetailComponent } from './album-detail/album-detail.component';
import { AlbumCreateComponent } from './album-create/album-create.component';
import { AlbumUpdateComponent } from './album-update/album-update.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    AlbumsListComponent,
    AlbumDetailComponent,
    AlbumCreateComponent,
    AlbumUpdateComponent
  ],
  imports: [
    CommonModule,
    AlbumsRoutingModule,
    SharedModule
  ],
})
export class AlbumsModule { }
