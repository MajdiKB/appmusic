export interface AlbumModelBack {
  _id: string;
  title: string;
  artistId: string;
  coverUrl: string;
  year: number;
  genre: string;
}

export interface AlbumModelFront {
  id: string;
  title: string;
  artist: string;
  photo: string;
  genre: string;
  year: number;
}
