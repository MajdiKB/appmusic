import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Services } from '../../../shared/services/services.service';

@Component({
  selector: 'app-album-create',
  templateUrl: './album-create.component.html',
  styleUrls: ['./album-create.component.scss'],
})
export class AlbumCreateComponent implements OnInit {
  public newAlbumForm: FormGroup = new FormGroup({});
  public submitted = false;
  public artistList: any[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private albumServices: Services,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.newAlbumForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      artistId: ['', [Validators.required]],
      coverUrl: [''],
      year: [''],
      genre: [''],
    });

    this.albumServices.getArtistsList().subscribe((data) => {
      this.artistList = data;
    });
    this.albumServices.goToTop();
  }

  public onSubmit(): void {
    this.submitted = true;
    if (this.newAlbumForm.valid) {
      const newAlbum: any = {
        title: this.newAlbumForm.get('title')?.value,
        artistId: this.newAlbumForm.get('artistId')?.value,
        coverUrl: this.newAlbumForm.get('coverUrl')?.value,
        year: this.newAlbumForm.get('year')?.value,
        genre: this.newAlbumForm.get('genre')?.value,
      };
      this.albumServices.newAlbum(newAlbum).subscribe(async () => {
        await this.router.navigate(['/albums/']);
      });
      this.newAlbumForm.reset();
      this.submitted = false;
    }
  }
}
