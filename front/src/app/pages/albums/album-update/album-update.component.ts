import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlbumModelBack } from '../models/album-model';
import { ArtistModelBack } from '../../artists/models/artist-model';
import { Services } from '../../../shared/services/services.service';

@Component({
  selector: 'app-album-update',
  templateUrl: './album-update.component.html',
  styleUrls: ['./album-update.component.scss'],
})
export class AlbumUpdateComponent implements OnInit {
  public updateAlbumForm: FormGroup = new FormGroup({});
  public submitted: boolean = false;
  public artistList: any[] = [];
  public albumId: string = '';
  public artist: ArtistModelBack = {} as ArtistModelBack;
  public albumDetailData: AlbumModelBack = {} as AlbumModelBack;

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private albumsServices: Services,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.albumId = params.get('id') as string;
    });
    this.getAlbumDetail(this.albumId);

    this.updateAlbumForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      artistId: ['', [Validators.required]],
      coverUrl: [''],
      year: [''],
      genre: [''],
    });

    // this.updateAlbumForm.controls['title'].setValue(this.albumDetailData.title);

    this.albumsServices.getArtistsList().subscribe((data) => {
      this.artistList = data;
    });
    this.albumsServices.goToTop();
  }

  public getAlbumDetail(albumId: string): void {
    this.albumsServices.getDetailAlbum(albumId).subscribe((data: any) => {
      this.albumDetailData = data;
      this.albumsServices
        .getArtistOfAlbum(this.albumDetailData.artistId)
        .subscribe((dataArtist: any) => {
          this.artist = dataArtist;
        });
    });
  }

  public onSubmit(): void {
    this.submitted = true;
    if (this.updateAlbumForm.valid) {
      const updateAlbum: any = {
        title: this.updateAlbumForm.get('title')?.value,
        artistId: this.updateAlbumForm.get('artistId')?.value,
        coverUrl: this.updateAlbumForm.get('coverUrl')?.value,
        year: this.updateAlbumForm.get('year')?.value,
        genre: this.updateAlbumForm.get('genre')?.value,
      };
      this.albumsServices
        .updateAlbum(updateAlbum, this.albumId)
        .subscribe(async () => {
          await this.router.navigate([`/albums/details/${this.albumId}`]);
        });
      this.updateAlbumForm.reset();
      this.submitted = false;
    }
  }
}
