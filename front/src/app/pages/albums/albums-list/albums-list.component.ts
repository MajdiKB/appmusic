import { Router } from '@angular/router';
import { AlbumModelFront } from '../models/album-model';
import { Component, OnInit } from '@angular/core';
import { Services } from '../../../shared/services/services.service';

@Component({
  selector: 'app-albums-list',
  templateUrl: './albums-list.component.html',
  styleUrls: ['./albums-list.component.scss'],
})
export class AlbumsListComponent implements OnInit {
  public albumList: AlbumModelFront[] = [];

  constructor(private albumsService: Services, private router: Router) {}

  ngOnInit(): void {
    this.getAlbumsList();
    this.albumsService.goToTop();
  }

  public getAlbumsList(): void {
    this.albumsService.getAlbumsList().subscribe((data: AlbumModelFront[]) => {
      const result: AlbumModelFront[] = data;
      this.albumList = result;
      this.albumList.forEach((album) => {
        this.albumsService.getArtistOfAlbum(album.artist).subscribe();
      });
    });
  }

  public deleteAlbum(albumId: string): void {
    this.albumsService.deleteAlbum(albumId).subscribe();
    this.router.navigate(['/albums']);
  }
}
