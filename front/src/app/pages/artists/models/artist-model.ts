export interface ArtistModelBack {
  _id: string;
  name: string;
  photoUrl: string;
  birthdate: Date;
  deathDate: Date;
}

export interface ArtistModelFront {
  id: string;
  name: string;
  photo: string;
}
