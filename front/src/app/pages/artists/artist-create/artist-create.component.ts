import { ArtistModelBack } from '../models/artist-model';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Services } from '../../../shared/services/services.service';

@Component({
  selector: 'app-artist-create',
  templateUrl: './artist-create.component.html',
  styleUrls: ['./artist-create.component.scss'],
})
export class ArtistCreateComponent implements OnInit {
  public newArtistForm: FormGroup = new FormGroup({});
  public submitted = false;
  public artist: ArtistModelBack = {} as ArtistModelBack;

  constructor(
    private formBuilder: FormBuilder,
    private artistsServices: Services,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.newArtistForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      photoUrl: [''],
      birthdate: [''],
      deathDate: [''],
    });
    this.artistsServices.goToTop();
  }

  public onSubmit(): void {
    this.submitted = true;
    if (this.newArtistForm.valid) {
      const newArtist: any = {
        name: this.newArtistForm.get('name')?.value,
        photoUrl: this.newArtistForm.get('photoUrl')?.value,
        birthdate: this.newArtistForm.get('birthdate')?.value,
        deathDate: this.newArtistForm.get('deathDate')?.value,
      };
      this.artistsServices.newArtist(newArtist).subscribe(async () => {
        await this.router.navigate([`/artists/`]);
      });
      this.newArtistForm.reset();
      this.submitted = false;
    }
  }
}
