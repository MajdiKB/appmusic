import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Services } from '../../../shared/services/services.service';
import { AlbumModelBack } from '../../albums/models/album-model';
import { ArtistModelBack } from '../models/artist-model';

@Component({
  selector: 'app-artist-details',
  templateUrl: './artist-details.component.html',
  styleUrls: ['./artist-details.component.scss'],
})
export class ArtistDetailsComponent implements OnInit {
  public artistId: string = '';
  public artistDetailData: ArtistModelBack = {} as ArtistModelBack;
  public artistAlbumsData: AlbumModelBack[] = [];
  public confirmDelete: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private artistsService: Services,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.artistId = params.get('id') as string;
      this.getArtistDetails(this.artistId);
      this.artistsService.goToTop();
    });
  }

  public getArtistDetails(artistId: string): void {
    this.artistsService
      .getArtistDetails(artistId)
      .subscribe((data: ArtistModelBack) => {
        this.artistDetailData = data;
      });
    this.artistsService.getAlbumsofArtist(artistId).subscribe((data: any) => {
      this.artistAlbumsData = data;
    });
  }

  public deleteArtist(artistId: string): any {
    this.artistsService
      .deleteArtist(artistId)
      .subscribe((err) => console.log(err));
    setTimeout((): void  => {
      this.router.navigate(['./artists/']);
    }, 200);
  }
}
