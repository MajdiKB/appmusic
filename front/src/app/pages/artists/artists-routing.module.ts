import { ArtistUpdateComponent } from './artist-update/artist-update.component';
import { ArtistsListComponent } from './artists-list/artists-list.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArtistDetailsComponent } from './artist-details/artist-details.component';
import { ArtistCreateComponent } from './artist-create/artist-create.component';

const routes: Routes = [
  { path: '', component: ArtistsListComponent },
  { path: 'new', component: ArtistCreateComponent },
  { path: 'update/:id', component: ArtistUpdateComponent },
  { path: 'details/:id', component: ArtistDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArtistsRoutingModule { }
