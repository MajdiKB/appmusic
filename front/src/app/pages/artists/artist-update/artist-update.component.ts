import { ArtistModelBack, ArtistModelFront } from '../models/artist-model';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Services } from '../../../shared/services/services.service';

@Component({
  selector: 'app-artist-update',
  templateUrl: './artist-update.component.html',
  styleUrls: ['./artist-update.component.scss'],
})
export class ArtistUpdateComponent implements OnInit {
  public updateArtistForm: FormGroup = new FormGroup({});
  public submitted: boolean = false;
  public artistId: string = '';
  public artist: ArtistModelBack = {} as ArtistModelBack;

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private artistsServices: Services,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.artistId = params.get('id') as string;
    });
    this.getArtistDetails(this.artistId);

    this.updateArtistForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      photoUrl: [''],
      birthdate: [''],
      deathDate: [''],
    });
    this.artistsServices.goToTop();
  }

  public getArtistDetails(artistId: string): void {
    this.artistsServices.getArtistDetails(artistId).subscribe((data: any) => {
      this.artist = data;
    });
  }

  public onSubmit(): void {
    this.submitted = true;
    if (this.updateArtistForm.valid) {
      const updateArtist: any = {
        name: this.updateArtistForm.get('name')?.value,
        photoUrl: this.updateArtistForm.get('photoUrl')?.value,
        birthdate: this.updateArtistForm.get('birthdate')?.value,
        deathDate: this.updateArtistForm.get('deathDate')?.value,
      };
      this.artistsServices
        .updateArtist(updateArtist, this.artistId)
        .subscribe(async () => {
          await this.router.navigate([`/artists/details/${this.artistId}`]);
        });
      this.updateArtistForm.reset();
      this.submitted = false;
    }
  }
}
