import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArtistsRoutingModule } from './artists-routing.module';
import { ArtistsListComponent } from './artists-list/artists-list.component';
import { ArtistDetailsComponent } from './artist-details/artist-details.component';
import { ArtistUpdateComponent } from './artist-update/artist-update.component';
import { ArtistCreateComponent } from './artist-create/artist-create.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    ArtistsListComponent,
    ArtistDetailsComponent,
    ArtistUpdateComponent,
    ArtistCreateComponent,
  ],
  imports: [
    CommonModule,
    ArtistsRoutingModule,
    SharedModule
  ],
})
export class ArtistsModule { }
