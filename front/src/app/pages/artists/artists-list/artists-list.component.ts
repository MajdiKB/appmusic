import { Services } from '../../../shared/services/services.service';
import { Component, OnInit } from '@angular/core';
import { ArtistModelFront } from '../models/artist-model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-artists-list',
  templateUrl: './artists-list.component.html',
  styleUrls: ['./artists-list.component.scss']
})
export class ArtistsListComponent implements OnInit {

  public artistList: ArtistModelFront[] = [];

  constructor(private artistsService: Services, private router: Router) { }

  ngOnInit(): void {
    this.getArtistList();
    this.artistsService.goToTop();
  }

  public getArtistList(): void{
    this.artistsService.getArtistsList().subscribe(
      (data: ArtistModelFront[]) => {
        const result: ArtistModelFront[] = data;
        this.artistList = result;
      }
    );
  }
}
