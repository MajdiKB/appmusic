import { Services } from '../../../shared/services/services.service';
import { Component, OnInit } from '@angular/core';
import { ArtistModelFront } from '../../artists/models/artist-model';
import { Router } from '@angular/router';
import { AlbumModelFront } from '../../albums/models/album-model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  public artistList: ArtistModelFront[] = [];
  public albumList: AlbumModelFront[] = [];
  public allItems: [] = [];
  constructor(private artistsService: Services, private router: Router) {}

  ngOnInit(): void {
    this.getArtistList();
    this.getAlbumsList();
  }

  public getArtistList(): void {
    this.artistsService
      .getArtistsList()
      .subscribe((data: ArtistModelFront[]) => {
        const result: ArtistModelFront[] = data;
        this.artistList = result;
      });
  }

  public getAlbumsList(): void {
    this.artistsService.getAlbumsList().subscribe((data: AlbumModelFront[]) => {
      const result: AlbumModelFront[] = data;
      this.albumList = result;
    });
  }
}
