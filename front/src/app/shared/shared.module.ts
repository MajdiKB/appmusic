import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Services } from './services/services.service';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  exports: [
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [Services]
})
export class SharedModule { }
