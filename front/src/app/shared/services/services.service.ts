import { ArtistModelFront } from '../../pages/artists/models/artist-model';
import { ArtistModelBack } from '../../pages/artists/models/artist-model';
import {
  AlbumModelBack,
  AlbumModelFront,
} from '../../pages/albums/models/album-model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';

@Injectable()
export class Services {
  public baseUrl: string = environment.apiUrl;

  constructor(private http: HttpClient, private router: Router) {}

  public getAlbumsList(): Observable<AlbumModelFront[]> {
    console.log(this.baseUrl);
    return this.http.get<AlbumModelBack>(this.baseUrl + 'albums/').pipe(
      map((res: any) => {
        const results: AlbumModelBack[] = res;
        const formattedResults: AlbumModelFront[] = results.map(
          ({ _id, title, artistId, coverUrl, genre, year }) => ({
            id: _id,
            title,
            artist: artistId,
            photo: coverUrl,
            genre,
            year,
          })
        );
        return formattedResults;
      }),
      catchError((err) => {
        const error = 'No connection to DB';
        this.router.navigate(['/error/', { error }]);
        throw new Error(err.message);
      })
    );
  }

  public getArtistOfAlbum(artistId: string): Observable<ArtistModelBack> {
    return this.http
      .get<ArtistModelBack>(this.baseUrl + 'artists/' + artistId)
      .pipe(
        map((res: any) => {
          const results: ArtistModelBack = res;
          return results;
        }),
        catchError((err) => {
          const error = 'No connection to DB';
          this.router.navigate(['/error/', { error }]);
          throw new Error(err.message);
        })
      );
  }

  public getDetailAlbum(albumId: string): Observable<AlbumModelBack> {
    return this.http
      .get<AlbumModelBack>(this.baseUrl + 'albums/' + albumId)
      .pipe(
        map((res: any) => {
          const results: AlbumModelBack = res;
          return results;
        }),
        catchError((err) => {
          const error = 'No connection to DB';
          this.router.navigate(['/error/', { error }]);
          throw new Error(err.message);
        })
      );
  }

  public getAlbumsofArtist(artistId: string): Observable<AlbumModelBack> {
    return this.http
      .get<AlbumModelBack>(this.baseUrl + 'albums/artist/' + artistId)
      .pipe(
        map((res: any) => {
          const results: AlbumModelBack = res;
          return results;
        }),
        catchError((err) => {
          const error = 'No connection to DB';
          this.router.navigate(['/error/', { error }]);
          throw new Error(err.message);
        })
      );
  }

  public newAlbum(newAlbum: {}): Observable<AlbumModelBack> {
    const headers = { 'content-type': 'application/json' };
    return this.http
      .post<AlbumModelBack>(this.baseUrl + 'albums', newAlbum, {
        headers,
      })
      .pipe(
        catchError((err) => {
          const error = 'Error creating a new album.';
          this.router.navigate(['/error/', { error }]);
          throw new Error(err.message);
        })
      );
  }

  public updateAlbum(
    updateAlbum: {},
    albumId: string
  ): Observable<AlbumModelBack> {
    const headers = { 'content-type': 'application/json' };
    return this.http
      .put<AlbumModelBack>(this.baseUrl + 'albums/' + albumId, updateAlbum, {
        headers,
      })
      .pipe(
        catchError((err) => {
          const error = 'Error trying update the album.';
          this.router.navigate(['/error/', { error }]);
          throw new Error(err.message);
        })
      );
  }

  public deleteAlbum(albumId: string): Observable<void> {
    return this.http.delete<void>(this.baseUrl + 'albums/' + albumId).pipe(
      catchError((err) => {
        const error = 'Error to delete.';
        this.router.navigate(['/error/', { error }]);
        throw new Error(err.message);
      })
    );
  }

  public getArtistsList(): Observable<ArtistModelFront[]> {
    return this.http.get<ArtistModelBack>(this.baseUrl + 'artists/').pipe(
      map((res: any) => {
        const results: ArtistModelBack[] = res;
        const formattedResults: ArtistModelFront[] = results.map(
          ({ _id, name, photoUrl }) => ({
            id: _id,
            name,
            photo: photoUrl,
          })
        );
        return formattedResults;
      }),
      catchError((err) => {
        const error = 'No connection to DB';
        this.router.navigate(['/error/', { error }]);
        throw new Error(err.message);
      })
    );
  }

  public getArtistDetails(artistId: string): Observable<ArtistModelBack> {
    return this.http
      .get<ArtistModelBack>(this.baseUrl + 'artists/' + artistId)
      .pipe(
        map((res: any) => {
          const results: ArtistModelBack = res;
          return results;
        }),
        catchError((err) => {
          const error = 'No connection to DB';
          this.router.navigate(['/error/', { error }]);
          throw new Error(err.message);
        })
      );
  }

  public newArtist(newArtist: {}): Observable<ArtistModelBack> {
    return this.http
      .post<ArtistModelBack>(this.baseUrl + 'artists', newArtist)
      .pipe(
        catchError((err) => {
          const error = 'Error creating a new artist.';
          this.router.navigate(['/error/', { error }]);
          throw new Error(err.message);
        })
      );
  }

  public updateArtist(
    artistUpdate: {},
    artistId: string
  ): Observable<ArtistModelBack> {
    const headers = { 'content-type': 'application/json' };
    return this.http
      .put<ArtistModelBack>(
        this.baseUrl + 'artists/' + artistId,
        artistUpdate,
        {
          headers,
        }
      )
      .pipe(
        catchError((err) => {
          const error = 'Error trying update the artist.';
          this.router.navigate(['/error/', { error }]);
          throw new Error(err.message);
        })
      );
  }

  public deleteArtist(artistId: string): Observable<void> {
    return this.http.delete<void>(this.baseUrl + 'artists/' + artistId).pipe(
      catchError((err) => {
        const error = 'Error to delete.';
        this.router.navigate(['/error/', { error }]);
        throw new Error(err.message);
      })
    );
  }

  public goToTop(): void {
    scroll(0, 0);
  }
}
