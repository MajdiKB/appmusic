import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {
  public message: string = "Error general.";
  constructor(private route: ActivatedRoute,private location: Location) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => this.message = params['error']);
  }

  public backClicked() {
    this.location.back();
  }
}
