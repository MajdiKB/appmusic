const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const artistSchema = new Schema(
  {
    name: { type: String, required: true  },
    photoUrl: { type: String },
    birthdate: {type: Date},
    deathDate: {type: Date},
  },
  {
    timestamps: true,
  }
);

const Artists = mongoose.model('Artists', artistSchema);
module.exports = Artists;