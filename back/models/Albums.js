const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const albumSchema = new Schema(
  {
    title: { type: String, required: true  },
    coverUrl: { type: String },
    year: { type: Number },
    genre: { type: String },
    artistId: [{ type: mongoose.Types.ObjectId, ref: 'Artist', required: true }],
  },
  {
    timestamps: true,
  }
);

const Album = mongoose.model('Album', albumSchema);

module.exports = Album;