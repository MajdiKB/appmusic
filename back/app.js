require('dotenv').config();
const express = require('express');
const cors = require('cors');
require('./config/db');

const path = require('path');
const session = require('express-session');
const MongoStore = require('connect-mongo');

const app = express();
const PORT = process.env.PORT || 3000;

const { DB_URL } = require('./config/db');

const homeRoute = require('./routes/home.route');
const artistsRoute = require('./routes/artists.route');
const albumsRoute = require('./routes/albums.route');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(
  session({
    // secret: process.env.SESSION_SECRET,
    secret: "upgrade-project",
    resave: false,
    saveUninitialized: false,
    cookie: {
      maxAge: 3600000,
    },
    store: MongoStore.create({
      mongoUrl: DB_URL,
    }),
  }),
);

var allowedOrigins = ['http://localhost:4200',
                      'https://appmusic-majdi.netlify.app'];
app.use(cors({
  origin: function(origin, callback){
    if(!origin) return callback(null, true);
    if(allowedOrigins.indexOf(origin) === -1){
      var msg = 'The CORS policy for this site does not ' +
                'allow access from the specified Origin.';
      return callback(new Error(msg), false);
    }
    return callback(null, true);
  }
}));

// app.use(cors({ credentials: true, origin: 'http://localhost:4200'}));
// app.use(cors({ credentials: true, origin: 'https://appmusic-majdi.netlify.app'}));

app.use('/', homeRoute);
app.use('/artists', artistsRoute);
app.use('/albums', albumsRoute);

app.use(express.static(path.join(__dirname, 'public')));

app.use('*', (req, res, next) => {
  const error = new Error('Route not found');
  error.status = 404;
  next(error); // Lanzamos la función next() con un error
});

app.use((err, req, res) => {
  return res.status(err.status || 500).render('error', {
    message: err.message || 'Unexpected error',
    status: err.status || 500,
  });
});

app.listen(PORT, () => {
  console.log(`Listening in http://localhost:${PORT}`);
});

