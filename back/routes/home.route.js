const express = require('express');

// eslint-disable-next-line new-cap
const router = express.Router();

router.get('/', (req, res, next) => {
  try {
    res.status(200).send('Welcome');
  } catch (err) {
    next(err);
  }
});

module.exports = router;
