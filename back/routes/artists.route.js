const express = require("express");
const { populate } = require("../models/Artists");
const MongoStore = require("connect-mongo");

// eslint-disable-next-line new-cap
const router = express.Router();

const Artists = require("../models/Artists");

router.get("/", async (req, res, next) => {
  try {
    const artist = await Artists.find();
    return res.status(200).json(artist);
  } catch (err) {
    next(err);
  }
});

router.get("/:id", async (req, res, next) => {
  try {
    const artistId = req.params.id;
    const artist = await Artists.findById(artistId);
    return res.status(200).json(artist);
  } catch (err) {
    return res.status(200).send("Error. No artist with this ID.");
  }
});

router.get("/name/:name", async (req, res, next) => {
  try {
    const artistName = req.params.name;
    const artist = await Artists.findOne({
      name: { $regex: `^${artistName}$`, $options: "i" },
    });
    if (artist) {
      return res.status(200).json(artist);
    } else {
      return res.status(200).send("Error. No artist with this name.");
    }
  } catch (err) {
    next(err);
  }
});

router.post("/", async (req, res, next) => {
  try {
    const artist = await Artists.create(req.body);
    return res.status(200).json(artist);
  } catch (err) {
    return res.status(200).send("Error creating a new artist.");
  }
});

router.delete("/:id", async (req, res, next) => {
  try {
    const artistId = req.params.id;
    const artist = await Artists.deleteOne({ _id: artistId });
    return res.status(200).json(`Artist with id: ${artistId} deleted`);
  } catch (err) {
    console.log(err);
    return res.status(200).send("Error. Artist no deleted");
  }
});

router.put(
  "/:id",
  async (req, res, next) => {
    try {
      const artistId = req.params.id;
      let artist = {};
      if (req.body.name) {
        artist.name = req.body.name;
      }
      if (req.body.photoUrl) {
        artist.photoUrl = req.body.photoUrl;
      }
      if (req.body.birthdate) {
        artist.birthdate = req.body.birthdate;
      }
      if (req.body.deathDate) {
        artist.deathDate = req.body.deathDate;
      }
      else if(!req.body.deathDate){
        artist.deathDate = "";
      }

      const updatedArtist = await Artists.findByIdAndUpdate(artistId, artist, {
        new: true,
      });

      // si usuario está vacio, significa q no hay nada q actualziar
      if (Object.keys(artist).length === 0 && artist.constructor === Object) {
        console.log("Ningún campo se ha actualizado.");
      } else {
        console.log("Artista actualizado correctamente.");
        console.log("Campos actualizados: ", artist);
      }
      return res.status(200).json(updatedArtist);
    } catch (err) {
      next(err);
    }
  }
);


module.exports = router;
