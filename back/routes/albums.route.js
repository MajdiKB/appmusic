const express = require('express');

// eslint-disable-next-line new-cap
const router = express.Router();

const Albums = require('../models/Albums');

router.get('/', async (req, res, next) => {
  try {
    const albums = await Albums.find().populate('albums');
    // para ordenar alfabeticamente
    albums.sort(function(a, b){
      if(a.title < b.title) { return -1; }
      if(a.title > b.title) { return 1; }
      return 0;
    })
    return res.status(200).json(albums);
  } catch (err) {
    next(err);
  }
});

router.get('/:id', async (req, res, next) => {
  try{
    const albumId = req.params.id;
    const album = await Albums.findById(albumId).populate('albums');
    return res.status(200).json(album);
  } catch(err){
    return res.status(200).send('Error. No album with this ID');
  }
})

router.get('/artist/:id', async (req, res, next) => {
  try {
    const idArtist = req.params.id;
    const album = await Albums.find({artistId: idArtist});
    if (album){
      return res.status(200).json(album);
    }
    else{
      return res.status(200).send('Error. No album with this title');
    }
  } catch (err) {
    next(err);
  }
});

router.post("/", async (req, res, next) => {
  try {
    const album = await Albums.create(req.body);
    return res.status(200).json(album);
  } catch (err) {
    return res.status(200).send("Error creating a new album.");
  }
});

router.delete("/:id", async (req, res, next) => {
  try {
    const albumId = req.params.id;
    const album = await Albums.deleteOne({ _id: albumId });
    return res.status(200).json(`Album with id: ${albumId} deleted`);
  } catch (err) {
    console.log(err);
    return res.status(200).send("Error. Album no deleted");
  }
});

router.put(
  "/:id",
  async (req, res, next) => {
    try {
      const albumId = req.params.id;
      let album = {};
      if (req.body.title) {
        album.title = req.body.title;
      }
      if (req.body.coverUrl) {
        album.coverUrl = req.body.coverUrl;
      }
      if (req.body.year) {
        album.year = req.body.year;
      }
      if (req.body.genre) {
        album.genre = req.body.genre;
      }
      if (req.body.artistId) {
        album.artistId = req.body.artistId;
      }

      const updatedAlbum = await Albums.findByIdAndUpdate(albumId, album, {
        new: true,
      });

      // si usuario está vacio, significa q no hay nada q actualziar
      if (Object.keys(album).length === 0 && album.constructor === Object) {
        console.log("Ningún campo se ha actualizado.");
      } else {
        console.log("Álbum actualizado correctamente.");
        console.log("Campos actualizados: ", album);
      }
      return res.status(200).json(updatedAlbum);
    } catch (err) {
      next(err);
    }
  }
);

module.exports = router;